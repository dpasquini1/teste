# Bem Vindo ao Projeto AlgarCRM

Os padrões e documentação do projeto se encontram na wiki no link http://intranet/wiki/index.php/Projeto_TI_B2C

## Efetuando o Build do projeto
Para efetuar o build do AlgarCRM basta entrar no diretorio source/ e digitar o comando abaixo:

`mvn clean install -nsu -Pdev` 

Caso ja tenha feito o build uma vez voce pode usar o comando baixo que e mais rapido (pois nao baix os modulos do nodejs para cada modulo)

`mvn clean install -nsu -Pdev -Dnpm.skip=true`

Não esqueça a opção -nsu (--no-snapshot-update) para que o seu workspace nao pegue indevidamente codigo de outras branches ou codigo antigo do archiva.

## Baixando o codigo fonte
1- Caso você ja tenha o acesso ao projeto primeiro você deve "baixar" o projeto isso é feito usando-se o clone do git como abaixo:

`git clone git@bitbucket.org:planetarepos/algarcrm.git`

Basta digitar o usuário e senha. Você já esta pronto para trabalhar no projeto

2- Caso você queira efetuar modificações no projeto algumas dicas:

2.1 - Sempre crie uma branch local antes de iniciar seu trabalho (isso te ajuda a mudar de tarefa mais facil caso seja necessario voltar a trabalhar no master)
Para criar uma branch local a partir do seu master digite:

git checkout -b feature-minha-branch

Altere os arquivos. A opção -b cria uma branch nova para você. Antes de efetuar o commit na branch local (ela esta apenas na sua maquina) voce deve versionar os arquivos. Use os comando abaixo:

git add .
git commit -m "meu primeiro commit"

Tome cuidado com o comando git add . pois ele adiciona (versiona) todos os arquivos não versionados ou alterados recursivamente a partir do diretório corrente. Para verificar
quais arquivos serão adicionados antes de usar o comando git add . as cegas use o git status.

git status

Desta forma você pode selecionar quais arquivos adicionar e commitar. Caso não queira digitar 2 comandos para efetuar o commit na sua branch local use o comando abaixo:

git commit -am "Meu primeiro commit com add"

Use o git status para verificar se não existe mais nada para ser commitado.

2.2 - Caso você queira fazer o push de sua branch local para o servidor remoto use o seguinte comando:

git push -u origin feature-minha-branch

utilizando a opção -u no comando push, além de atualizar o repositório remoto com sua branch, você também especifica que sua feature-minha-branch estará relacionada à branch origin/feature-minha-branch (no servidor). Dessa maneira, localmente estando em sua branch quando você executar o comando git push, suas alterações serão adicionadas diretamente na branch remota origin/feature-minha-branch sem que seja necessário especificar explicitamente a branch remota referente ao push

3- Efetuando o Merge 

Após ter terminado seu trabalho  na sua feature-branch-local é hora de efetuar o merge na master. Para isso use os comando abaixo:

Primeiro va para a branch master:

git checkout master

Apos isso o comando:

git merge feature-minha-branch

Pronto você esta pronto para efetar o push das suas mudança para o "servidor" central do gitlab (10.51.25.177). Antes verifique se alguem alterou o master:

git pull

O comando acima ira trazer as atualizacoes do servidor e ja fazer o merge automatico. Agora voce pode efetuar o "upload" das suas atualizacoes para o servidor central:

git push

Esses são os principais comandos que você precisa no seu dia a dia. Outro mais comum e util e o git log muito embora sugiro usar a propria interface grafica do |GitLab que apresenta 
os logs e historico de forma facil.

4- Deletando uma branch

Apos finalizar o trabalho dentro de uma branch deve efetuar o merge e SEMPRE remover a branch (seja ela local e a remota) para evitar que o repositorio se torne um lixo de 
branchs inuteis. 

Para remover uma branch local use o comando abaixo:

git branch -d minha-branch

Para remover uma branch do repositorio remote use o comando abaixo:

git push origin :minha-branch

5- "Baixando" uma branch remota

Utilize o comando abaixo:

git checkout --track -b minha-branch origin/minha-branch

6- Limpando o workspace Git 
Muitas vezes, quando você está trabalhando em uma parte do seu projeto, as coisas estão em um estado confuso e você quer mudar de branch por um tempo para trabalhar em outra coisa. O problema é, você não quer fazer o commit de um trabalho incompleto somente para voltar a ele mais tarde. A resposta para esse problema é o comando git stash.
Caso voce queira limpar o seu workspace Git para fazer um pull mas ainda nao quer comitar as alteracoes voce pode usar o comando abaixo.
 git stash
 
Com isso voce tera as alteracoes salvas em um cache do Git limpando seu workspace. Abaixo voce pode listar as suas alteracoes no stash 
 git stash list
 
Para aplicar as alteracoes na branch q voce montou use o comando abaixo.
 git stash apply --index
 
 
Pra limpar o stash aplicado use o comando abaixo.
 git stash drop


Para trocar o remote de HTTP para SSH
- Gerar uma ssh-key com o comando ssh-keygen e incluí-la no GitLab em Profile Settings
- Ver remote atual:
  git remote -v
- Alterar o remote:
  git remote set-url origin git@10.51.25.177:root/algarcrm.git


Para maiores informações consulte o livro on-line (free) https://git-scm.com/book/en/v2/

Duvidas por favor entrem em contato.
